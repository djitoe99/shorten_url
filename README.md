- create database shortage;
- create 2 table :
- 

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `url_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_target` text,
  `url_bitly` varchar(200) DEFAULT NULL,
  `desc` text,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


- vhost:
- sampai folder /shortage
- 
<VirtualHost *:80>
    ServerAdmin jati@test.com
    DocumentRoot "/var/www/html/bitly/shortage"
    ServerName bitly.dev
<Directory "/var/www/html/bitly/shortage">
    RewriteEngine on
       # If a directory or a file exists, use the request directly
       RewriteCond %{REQUEST_FILENAME} !-f
       RewriteCond %{REQUEST_FILENAME} !-d
       # Otherwise forward the request to index.php
       RewriteRule . index.php
       # use index.php as index file
       DirectoryIndex index.php
    #php_admin_value open_basedir "/usr/local/www/yii:/tmp/"
    Options  -Indexes +FollowSymLinks +Includes +ExecCGI
    AllowOverride All
    Require all granted
</Directory>
    <IfModule mod_php5.c>
        php_admin_value open_basedir "/var/www/html/bitly:/usr/local/lib/php:/tmp"
    </IfModule>
</VirtualHost>

