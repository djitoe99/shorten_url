<?php

class BitlyController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Bitly;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bitly']))
		{
			$model->attributes=$_POST['Bitly'];
                        $urlShortedData = $this->CompressURL($model->url_target,'djitoe','R_8d5d2947a2634be2960b2229d3bfc85f','json');
                        if($urlShortedData){
                            $model->url_bitly = $urlShortedData['shortUrl'];
                        }
                        $model->created_date = date('Y-m-d');
			if($model->save())
				$this->redirect(array('admin','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bitly']))
		{
			$model->attributes=$_POST['Bitly'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Bitly');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Bitly('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Bitly']))
			$model->attributes=$_GET['Bitly'];

                $groups = $this->getBitLinks('djitoe','djitoe','R_8d5d2947a2634be2960b2229d3bfc85f', 'clicks');
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Bitly the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Bitly::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Bitly $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bitly-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function getBitLinks($groupId, $login, $appkey, $sort = 'clicks') {

          $ch = curl_init();
          
          $sUrl = 'https://api-ssl.bitly.com/v4/groups/'.$groupId.'/bitlinks';
          curl_setopt($ch, CURLOPT_URL, $sUrl);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
          curl_setopt($ch, CURLOPT_HEADER, 0);

          $response = curl_exec ($ch);
          curl_close ($ch);
          
          die(print_r($response));
          $json = @json_decode($response,true);
          if($json){
              if(($json['errorCode']) == 0 && isset($json['results'][$url])){
                  return $json['results'][$url];
              }
          }
          return false;

        }        
        
        public function CompressURL($url,$login,$appkey,$format = 'xml',$version = '2.0.1') {

          $ch = curl_init();
          
          $sUrl = 'http://api.bit.ly/shorten?version='.$version.'&longUrl='.urlencode($url).'&login='.$login.'&apiKey='.$appkey.'&format='.$format;
          curl_setopt($ch, CURLOPT_URL, $sUrl);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
          curl_setopt($ch, CURLOPT_HEADER, 0);

          $response = curl_exec ($ch);
          curl_close ($ch);
          
          $json = @json_decode($response,true);
          if($json){
              if(($json['errorCode']) == 0 && isset($json['results'][$url])){
                  return $json['results'][$url];
              }
          }
          return false;

        }        
}
