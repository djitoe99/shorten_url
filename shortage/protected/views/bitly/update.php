<?php
/* @var $this BitlyController */
/* @var $model Bitly */

$this->breadcrumbs=array(
	'Bitlies'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage Bitly', 'url'=>array('admin')),
	array('label'=>'Create Bitly', 'url'=>array('create')),
	array('label'=>'View Bitly', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1>Update Bitly <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>