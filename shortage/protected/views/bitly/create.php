<?php
/* @var $this BitlyController */
/* @var $model Bitly */

$this->breadcrumbs=array(
	'Bitlies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Bitly', 'url'=>array('admin')),
);
?>

<h1>Create Bitly</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>