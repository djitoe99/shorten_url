<?php
/* @var $this BitlyController */
/* @var $model Bitly */

$this->breadcrumbs=array(
	'Bitlies'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Bitly', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bitly-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Statistik URL</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bitly-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'desc',
		'url_target',
		'url_bitly',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
