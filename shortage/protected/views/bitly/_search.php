<?php
/* @var $this BitlyController */
/* @var $model Bitly */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>



	<div class="row">
		<?php echo $form->label($model,'url_target'); ?>
		<?php echo $form->textField($model,'url_target'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'url_bitly'); ?>
		<?php echo $form->textField($model,'url_bitly',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->