<?php
/* @var $this BitlyController */
/* @var $data Bitly */
?>

<div class="view">



	<b><?php echo CHtml::encode($data->getAttributeLabel('url_target')); ?>:</b>
	<?php echo CHtml::encode($data->url_target); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url_bitly')); ?>:</b>
	<?php echo CHtml::encode($data->url_bitly); ?>
	<br />




</div>