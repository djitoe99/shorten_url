<?php
/* @var $this BitlyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bitlies',
);

$this->menu=array(
	array('label'=>'Create Bitly', 'url'=>array('create')),
	array('label'=>'Manage Bitly', 'url'=>array('admin')),
);
?>

<h1>Bitlies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
