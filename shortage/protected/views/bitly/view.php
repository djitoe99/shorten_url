<?php
/* @var $this BitlyController */
/* @var $model Bitly */

$this->breadcrumbs=array(
	'Bitlies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Bitly', 'url'=>array('index')),
	array('label'=>'Create Bitly', 'url'=>array('create')),
	array('label'=>'Update Bitly', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Bitly', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bitly', 'url'=>array('admin')),
);
?>

<h1>View Bitly #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'url_target',
		'url_bitly',
		'created_date',
	),
)); ?>
